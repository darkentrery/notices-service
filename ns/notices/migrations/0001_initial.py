# Generated by Django 5.0.1 on 2024-01-03 12:35

import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Client",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("phone", models.CharField(max_length=11, verbose_name="Phone number")),
                (
                    "phone_code",
                    models.CharField(
                        max_length=5, verbose_name="Code of Mobile Operator"
                    ),
                ),
                ("tag", models.CharField(max_length=250, verbose_name="Tag")),
                ("timezone", models.CharField(max_length=4, verbose_name="Timezone")),
            ],
            options={
                "verbose_name": "Client",
                "verbose_name_plural": "Clients",
            },
        ),
        migrations.CreateModel(
            name="Newsletter",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("begin_time", models.DateTimeField(verbose_name="Begin Time")),
                ("end_time", models.DateTimeField(verbose_name="End Time")),
                ("text", models.TextField(verbose_name="Text")),
                (
                    "filter_tag",
                    models.CharField(
                        default="", max_length=255, verbose_name="Tag Filter"
                    ),
                ),
                (
                    "filter_phone_code",
                    models.CharField(
                        default="", max_length=255, verbose_name="Phone Code Filter"
                    ),
                ),
            ],
            options={
                "verbose_name": "Newsletter",
                "verbose_name_plural": "Newsletters",
            },
        ),
        migrations.CreateModel(
            name="Message",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name="Created At"
                    ),
                ),
                (
                    "status",
                    models.CharField(
                        choices=[("created", "created"), ("send", "send")],
                        default="created",
                        max_length=50,
                        verbose_name="Status",
                    ),
                ),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="messages",
                        to="notices.client",
                    ),
                ),
                (
                    "newsletter",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="messages",
                        to="notices.newsletter",
                    ),
                ),
            ],
            options={
                "verbose_name": "Message",
                "verbose_name_plural": "Messages",
            },
        ),
    ]
