import re

from rest_framework import serializers

from ns.notices.models import Client, Newsletter, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ["id", "phone", "phone_code", "tag", "timezone"]
        read_only_fields = fields


def phone(value):
    if not value.get("phone"):
        raise serializers.ValidationError("Phone field is required!")
    phone = re.search(r'\D{1,}', value.get("phone"))
    if phone:
        raise serializers.ValidationError("Phone field may contain only digits!")
    if len(value.get("phone")) != 11:
        raise serializers.ValidationError("Phone field must contain 11 digits!")
    if value.get("phone")[0] != "7":
        raise serializers.ValidationError("Phone field must begin with '7'!")


class CreateClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ["phone", "phone_code", "tag", "timezone"]
        validators = [phone,]


class UpdateClientSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Client
        fields = ["id", "phone", "phone_code", "tag", "timezone"]
        validators = [phone,]


class NewsletterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = ["id", "begin_time", "end_time", "text", "filter_tag", "filter_phone_code", "begin_period", "end_period"]
        read_only_fields = fields


class CreateNewsletterSerializer(serializers.ModelSerializer):
    begin_period = serializers.TimeField(required=False, allow_null=True)
    end_period = serializers.TimeField(required=False, allow_null=True)

    class Meta:
        model = Newsletter
        fields = ["begin_time", "end_time", "text", "filter_tag", "filter_phone_code", "begin_period", "end_period"]


class UpdateNewsletterSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(write_only=True)
    begin_period = serializers.TimeField(required=False, allow_null=True)
    end_period = serializers.TimeField(required=False, allow_null=True)

    class Meta:
        model = Newsletter
        fields = ["id", "begin_time", "end_time", "text", "filter_tag", "filter_phone_code", "begin_period", "end_period"]


class StatNewslettersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = ["id", "created_messages", "send_messages"]
        read_only_fields = fields


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ["id", "created_at", "status", "client"]
        read_only_fields = fields
