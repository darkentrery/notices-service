from django.db import models
from django.utils import timezone


class Newsletter(models.Model):
    begin_time = models.DateTimeField("Begin Time")
    end_time = models.DateTimeField("End Time")
    text = models.TextField("Text")
    filter_tag = models.CharField("Tag Filter", max_length=255, default="")
    filter_phone_code = models.CharField("Phone Code Filter", max_length=255, default="")
    begin_period = models.TimeField("Begin of Available period", blank=True, null=True)
    end_period = models.TimeField("End of Available period", blank=True, null=True)

    class Meta:
        verbose_name = "Newsletter"
        verbose_name_plural = "Newsletters"

    def __str__(self):
        return f"{self.begin_time} - {self.end_time}"

    @property
    def created_messages(self) -> int:
        return self.messages.filter(status=Message.MessageStatus.SEND).count()

    @property
    def send_messages(self) -> int:
        return self.messages.filter(status=Message.MessageStatus.CREATED).count()


class Client(models.Model):
    phone = models.CharField("Phone number", max_length=11)
    phone_code = models.CharField("Code of Mobile Operator", max_length=5)
    tag = models.CharField("Tag", max_length=250)
    timezone = models.FloatField("Timezone By UTC", default=0)

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Clients"

    def __str__(self):
        return f"{self.phone_code} {self.phone}"


class Message(models.Model):
    class MessageStatus(models.TextChoices):
        CREATED = "created", "created"
        SEND = "send", "send"

    created_at = models.DateTimeField("Created At", default=timezone.now)
    status = models.CharField("Status", max_length=50, choices=MessageStatus.choices, default=MessageStatus.CREATED)
    newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE, related_name="messages")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="messages")

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"

    def __str__(self):
        return f"{self.newsletter.__str__()} {self.client.__str__()}"
