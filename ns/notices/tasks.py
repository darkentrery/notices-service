import datetime
import time

import requests
from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
from django.utils import timezone
from loguru import logger

from ns.notices.models import Newsletter, Message


@shared_task()
@logger.catch
def start_newsletter_task(newsletter_id: int) -> None:
    instance = Newsletter.objects.get(id=newsletter_id)
    now = timezone.now()
    wait = (instance.begin_time - now).total_seconds()
    if wait > 0:
        time.sleep(wait)

    while instance.messages.filter(status=Message.MessageStatus.CREATED).exists() and instance.end_time >= timezone.now():
        for message in instance.messages.filter(status=Message.MessageStatus.CREATED):
            if instance.begin_period and instance.end_period:
                now = datetime.datetime.utcnow()
                begin = datetime.datetime.combine(now.date(), instance.begin_period)
                end = datetime.datetime.combine(now.date(), instance.end_period)
                now = now + datetime.timedelta(minutes=message.client.timezone * 60)
                if begin <= now <= end:
                    res = requests.post(
                        f"https://probe.fbrq.cloud/v1/send/{message.id}",
                        headers={"Authorization": f"Bearer {settings.SENDER_TOKEN}",
                                 "Content-Type": "application/json"},
                        json={"id": message.id, "phone": message.client.phone, "text": message.newsletter.text}
                    )
                    if res.status_code == 200:
                        message.status = Message.MessageStatus.SEND
                        message.save()
                time.sleep(1)
            else:
                res = requests.post(
                    f"https://probe.fbrq.cloud/v1/send/{message.id}",
                    headers={"Authorization": f"Bearer {settings.SENDER_TOKEN}", "Content-Type": "application/json"},
                    json={"id": message.id, "phone": message.client.phone, "text": message.newsletter.text}
                )
                if res.status_code == 200:
                    message.status = Message.MessageStatus.SEND
                    message.save()


@shared_task(name="daily_report_task")
@logger.catch
def daily_report_task() -> None:
    created = 0
    send = 0
    for item in Newsletter.objects.all():
        created += item.created_messages
        send += item.send_messages

    subject = "daily report"
    message_body = f"Created {created} messages, send {send} messages."
    send_mail(subject, message_body, settings.EMAIL_HOST_USER, [settings.ADMIN_EMAIL])
