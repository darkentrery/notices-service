# from django.db import models
# from django.utils import timezone
# from django.utils.translation import gettext_lazy as _
#
#
# class CeleryTask(models.Model):
#     process_id = models.CharField(_("Process Id"), max_length=150)
#     newsletter = models.ForeignKey("notices.Newsletter", on_delete=models.CASCADE, related_name="tasks")
#     in_process = models.BooleanField(_("In Process"), default=False)
#     created_at = models.DateTimeField(_("Created At"), default=timezone.now)
#
#     class Meta:
#         verbose_name = "Celery Task"
#         verbose_name_plural = "Celery Tasks"
#
#     def __str__(self):
#         return f"{self.newsletter.__str__()}"
