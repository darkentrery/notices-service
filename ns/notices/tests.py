from django.db.models import Q
from django.test import TestCase
from django.utils import timezone

from ns.notices.models import Client, Newsletter, Message


class ClientTests(TestCase):
    def test_post(self):
        response = self.client.post(
            "/api/client/",
            {
                "phone": "78888888899",
                "phone_code": "+7",
                "tag": "st",
                "timezone": 0
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        client = response.json()
        self.assertEqual(client["phone"], "78888888899")

    def test_put(self):
        client = Client.objects.create(
            phone="78888888899",
            phone_code="+7",
            tag="st",
            timezone=0
        )
        response = self.client.put(
            "/api/client/",
            {
                "id": client.id,
                "phone": client.phone[:-1] + "4",
                "phone_code": client.phone_code,
                "tag": client.tag,
                "timezone": client.timezone
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        client = response.json()
        self.assertEqual(client["phone"], "78888888894")

    def test_delete(self):
        client = Client.objects.create(
            phone="78888888899",
            phone_code="+7",
            tag="st",
            timezone=0
        )
        response = self.client.delete(
            f"/api/client/?id={client.id}",
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)


class NewsletterTests(TestCase):
    def test_post(self):
        response = self.client.post(
            "/api/newsletter/",
            {
                "begin_time": "2024-01-05T10:15:49.340Z",
                "end_time": "2024-01-05T10:15:49.340Z",
                "text": "string",
                "filter_tag": "string",
                "filter_phone_code": "string",
                "begin_period": "10:00:00",
                "end_period": "11:00:00",
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        newsletter = response.json()
        self.assertEqual(newsletter["text"], "string")

    def test_put(self):
        newsletter = Newsletter.objects.create(
            begin_time=timezone.now(),
            end_time=timezone.now(),
            text="string",
            filter_tag="string",
            filter_phone_code="string",
            begin_period="10:00:00",
            end_period="11:00:00"
        )
        response = self.client.put(
            "/api/newsletter/",
            {
                "id": newsletter.id,
                "begin_time": "2024-01-05T10:15:49.340Z",
                "end_time": "2024-01-05T10:15:49.340Z",
                "text": "string1",
                "filter_tag": "string",
                "filter_phone_code": "string",
                "begin_period": "10:00:00",
                "end_period": "11:00:00",
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        newsletter = response.json()
        self.assertEqual(newsletter["text"], "string1")

    def test_delete(self):
        newsletter = Newsletter.objects.create(
            begin_time=timezone.now(),
            end_time=timezone.now(),
            text="string",
            filter_tag="string",
            filter_phone_code="string",
            begin_period="10:00:00",
            end_period="11:00:00"
        )
        response = self.client.delete(
            f"/api/newsletter/?id={newsletter.id}",
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)

    def test_get(self):
        response = self.client.get(
            f"/api/newsletters/",
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        newsletter = response.json()
        self.assertEqual(type(newsletter), list)


class MessageTests(TestCase):
    def test_get(self):
        newsletter = Newsletter.objects.create(
            begin_time=timezone.now(),
            end_time=timezone.now(),
            text="st",
            filter_tag="string",
            filter_phone_code="string",
            begin_period="10:00:00",
            end_period="11:00:00"
        )
        Client.objects.create(
            phone="78888888899",
            phone_code="+7",
            tag="st",
            timezone=0
        )
        qs_filter = Q()
        if newsletter.filter_tag:
            qs_filter |= Q(tag__in=newsletter.filter_tag.split(","))
        if newsletter.filter_phone_code:
            qs_filter |= Q(phone_code__in=newsletter.filter_phone_code.split(","))
        clients = Client.objects.filter(qs_filter)
        for client in clients:
            Message.objects.create(newsletter=newsletter, client=client)
        response = self.client.get(
            f"/api/messages/?newsletter_id={newsletter.id}",
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        messages = response.json()
        self.assertEqual(type(messages), list)
