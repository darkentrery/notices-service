from django.db.models import Q
from drf_spectacular.utils import extend_schema, OpenApiParameter
from loguru import logger
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from ns.notices.models import Client, Newsletter, Message
from ns.notices.serializers import (
    CreateClientSerializer,
    ClientSerializer,
    UpdateClientSerializer,
    CreateNewsletterSerializer,
    NewsletterSerializer,
    UpdateNewsletterSerializer,
    StatNewslettersSerializer,
    MessageSerializer
)
from ns.notices.tasks import start_newsletter_task


class ClientView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        request=CreateClientSerializer,
        responses={201: ClientSerializer},
        description="В поле 'timezone' укажите часовой пояс относительно UTC в часах.",
    )
    def post(self, request, **kwargs):
        logger.info(f"{request.data=}")
        serializer = CreateClientSerializer(data=request.data)
        if serializer.is_valid():
            client = serializer.save()
            if client:
                json = ClientSerializer(instance=client).data
                logger.info(f"{json=}")
                return Response(json, status=status.HTTP_200_OK)
        logger.debug(f"{serializer.errors=}")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(
        request=UpdateClientSerializer,
        responses={200: ClientSerializer},
        description="В поле 'timezone' укажите часовой пояс относительно UTC в часах.",
    )
    def put(self, request, **kwargs):
        try:
            logger.info(f"{request.data=}")
            instance = Client.objects.get(id=request.data.get("id"))
        except:
            return Response({"Message": "No valid id!"}, status=status.HTTP_400_BAD_REQUEST)
        serializer = UpdateClientSerializer(instance=instance, data=request.data)
        if serializer.is_valid():
            client = serializer.save()
            if client:
                json = ClientSerializer(instance=client).data
                logger.info(f"{json=}")
                return Response(json, status=status.HTTP_200_OK)
        logger.debug(f"{serializer.errors=}")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(
        parameters=[
            OpenApiParameter(name="id", location=OpenApiParameter.QUERY, required=True, type=int),
        ],
        responses={200: None},
    )
    def delete(self, request, **kwargs):
        try:
            logger.info(f"{request.query_params=}")
            instance = Client.objects.get(id=request.query_params.get("id"))
            instance.delete()
            return Response(status=status.HTTP_200_OK)
        except:
            logger.debug(f"{request.query_params=}")
            return Response({"Message": "No valid id!"}, status=status.HTTP_400_BAD_REQUEST)


class NewsletterView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        request=CreateNewsletterSerializer,
        responses={201: NewsletterSerializer},
        description="В полях 'filter_tag' и 'filter_phone_code' перечислите через запятую теги для филтрации по ним пользователей.\n"
                    "Поля 'begin_period' и 'end_period' я вляются не обязательными. Их можно удалить или указать как null."
    )
    def post(self, request, **kwargs):
        serializer = CreateNewsletterSerializer(data=request.data)
        if serializer.is_valid():
            item: Newsletter = serializer.save()
            if item:
                qs_filter = Q()
                if item.filter_tag:
                    qs_filter |= Q(tag__in=item.filter_tag.split(","))
                if item.filter_phone_code:
                    qs_filter |= Q(phone_code__in=item.filter_phone_code.split(","))
                clients = Client.objects.filter(qs_filter)
                for client in clients:
                    Message.objects.create(newsletter=item, client=client)

                start_newsletter_task.apply_async(args=[item.id], countdown=3)

                json = NewsletterSerializer(instance=item).data
                logger.info(f"{json=}")
                return Response(json, status=status.HTTP_200_OK)
        logger.debug(f"{serializer.errors=}")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(
        request=UpdateNewsletterSerializer,
        responses={200: NewsletterSerializer},
        description="В полях 'filter_tag' и 'filter_phone_code' перечислите через запятую теги для филтрации по ним пользователей.\n"
                    "Поля 'begin_period' и 'end_period' я вляются не обязательными. Их можно удалить или указать как null."
    )
    def put(self, request, **kwargs):
        try:
            instance = Newsletter.objects.get(id=request.data.get("id"))
        except:
            return Response({"Message": "No valid id!"}, status=status.HTTP_400_BAD_REQUEST)
        serializer = UpdateNewsletterSerializer(instance=instance, data=request.data)
        if serializer.is_valid():
            item = serializer.save()
            if item:
                json = NewsletterSerializer(instance=item).data
                logger.info(f"{json=}")
                return Response(json, status=status.HTTP_200_OK)
        logger.debug(f"{serializer.errors=}")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @extend_schema(
        parameters=[
            OpenApiParameter(name="id", location=OpenApiParameter.QUERY, required=True, type=int),
        ],
        responses={200: None},
    )
    def delete(self, request, **kwargs):
        try:
            instance = Newsletter.objects.get(id=request.query_params.get("id"))
            instance.delete()
            return Response(status=status.HTTP_200_OK)
        except:
            logger.debug(f"{request.query_params=}")
            return Response({"Message": "No valid id!"}, status=status.HTTP_400_BAD_REQUEST)


class NewslettersView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        responses={200: StatNewslettersSerializer(many=True)},
    )
    @logger.catch
    def get(self, request, **kwargs):
        json = StatNewslettersSerializer(Newsletter.objects.all(), many=True).data
        logger.info(f"{json=}")
        return Response(json, status=status.HTTP_200_OK)


class MessagesView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        parameters=[
            OpenApiParameter(name="newsletter_id", location=OpenApiParameter.QUERY, required=True, type=int),
        ],
        responses={200: MessageSerializer(many=True)},
    )
    def get(self, request, **kwargs):
        logger.info(f"{request.query_params=}")
        try:
            instance = Newsletter.objects.get(id=request.query_params.get("newsletter_id"))
        except:
            logger.debug(f"{request.query_params=}")
            return Response({"Message": "No valid id!"}, status=status.HTTP_400_BAD_REQUEST)

        json = MessageSerializer(instance.messages.all(), many=True).data
        logger.info(f"{json=}")
        return Response(json, status=status.HTTP_200_OK)
