from django.urls import path

from ns.notices import views

urlpatterns = [
    path("client/", views.ClientView.as_view()),
    path("newsletter/", views.NewsletterView.as_view()),
    path("newsletters/", views.NewslettersView.as_view()),
    path("messages/", views.MessagesView.as_view()),
]
