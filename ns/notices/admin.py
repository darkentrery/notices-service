from django.contrib import admin

from ns.notices.models import Newsletter, Client, Message


class MessageInline(admin.TabularInline):
    model = Message
    extra = 0
    fields = [
        "id",
        "created_at",
        "status",
        "newsletter",
        "client",
    ]
    readonly_fields = ["id"]


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    list_display = [
        "begin_time",
        "end_time",
        "text",
    ]
    inlines = [MessageInline]


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "phone_code",
        "phone",
    ]


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = [
        "created_at",
        "status",
        "newsletter",
        "client",
    ]
